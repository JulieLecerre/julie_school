const AWS = require("aws-sdk");

const dynamo = new AWS.DynamoDB.DocumentClient();

function uuid(){
    var dt = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (dt + Math.random()*16)%16 | 0;
        dt = Math.floor(dt/16);
        return (c=='x' ? r :(r&0x3|0x8)).toString(16);
    });
    return uuid;
}



exports.handler = async (event, context) => {
  let body;
  let statusCode = 200;
  const headers = {
    "Content-Type": "application/json",
    "Access-Control-Allow-Headers" : "Content-Type",
    "Access-Control-Allow-Origin": "*",
    "Access-Control-Allow-Methods": "OPTIONS,POST,GET"
  };

let requestJSON;

try {
    switch (event.routeKey) {
        case "POST /classroom":
            let id = uuid();
            
            requestJSON = JSON.parse(event.body);
            await dynamo
            .put({
                TableName: "classrooms",
                Item: {
                    id: id,
                    id_students: requestJSON.id_students,
                    niveau: requestJSON.niveau
                }
            })
            .promise();
            body = id;
            break;
            
        case "POST /classroom/{id}":
            requestJSON = JSON.parse(event.body);
            await dynamo
            .put({
                TableName: "classrooms",
                Item: {
                    id: event.pathParameters.id,
                    id_students: requestJSON.id_students,
                    niveau: requestJSON.niveau
                }
            })
            .promise();
            break;
        case "GET /classroom":
            let classrooms = await dynamo.scan({ TableName: "classrooms" }).promise();
            body = classrooms["Items"];
            break;
        case "GET /classroom/{id}":
            body = await dynamo
             .get({
                TableName: "classrooms",
                Key: {
                  id: event.pathParameters.id
                }
            })
            .promise();
            break;
        case "DELETE /classroom/{id}":
            let classroom = await dynamo
             .get({
                TableName: "classrooms",
                Key: {
                  id: event.pathParameters.id
                }
            })
            .promise();
            
            let isClassroomExist = !(classroom 
                && Object.keys(classroom).length === 0
                && Object.getPrototypeOf(classroom) === Object.prototype);
                
            if (isClassroomExist) {
                let id_students = classroom["Item"]["id_students"];
                if (id_students.length === 0) {
                    await dynamo
                        .delete({
                            TableName: "classrooms",
                            Key: {
                                id: event.pathParameters.id
                            }
                        })
                    .promise();
                    body = `Deleted classroom ${event.pathParameters.id}`;
                }
                else {
                    statusCode = 500;
                    body=`Cannot delete classroom ${event.pathParameters.id} because id_students must be empty`;
                }
            }
            else {
                statusCode = 500;
                body = `Cannot find classroom ${event.pathParameters.id}`;
            }
            break;

        case "POST /student":
            let id_student = uuid();
            requestJSON = JSON.parse(event.body);
            await dynamo
            .put({
                TableName: "students",
                Item: {
                    id:id_student,
                    firstName: requestJSON.firstName,
                    lastName: requestJSON.lastName,
                    email: requestJSON.email,
                    age: requestJSON.age,
                    gender: requestJSON.gender,
                    promo: requestJSON.promo,
                    notation: requestJSON.notation,
                    speciality: requestJSON.speciality,
                    idClassroom:requestJSON.idClassroom,
           
                }
            })
            .promise();
            body = id_student;
            break;
            
        case "POST /student/{id}":
            requestJSON = JSON.parse(event.body);
            await dynamo
            .put({
                TableName: "students",
                Item: {
                    id:event.pathParameters.id,
                    firstName: requestJSON.firstName,
                    lastName: requestJSON.lastName,
                    email: requestJSON.email,
                    age: requestJSON.age,
                    gender: requestJSON.gender,
                    promo: requestJSON.promo,
                    notation: requestJSON.notation,
                    speciality: requestJSON.speciality,
                    idClassroom:requestJSON.idClassroom,
           
                }
            })
            .promise();
            break;
        case "GET /student":
            let students = await dynamo.scan({ TableName: "students" }).promise();
            body = students["Items"];
            break;
        case "GET /student/{id}":
            body = await dynamo
             .get({
                TableName: "students",
                Key: {
                  id: event.pathParameters.id
                }
            })
            .promise();
            break;

        case "DELETE /student/{id}":
  
            let studentInfo = await dynamo
             .get({
                TableName: "students",
                Key: {
                  id: event.pathParameters.id
                }
            })
            .promise();
            
            let idClassroom= studentInfo["Item"]["idClassroom"];
            
            let classroomInfo = await dynamo
             .get({
                TableName: "classrooms",
                Key: {
                  id: idClassroom
                }
            })
            .promise();
            
            let classroomData = classroomInfo["Item"];
            
            var index = classroomData["id_students"].indexOf(event.pathParameters.id);
            if (index !== -1) {
              classroomData["id_students"].splice(index, 1);
            }
            await dynamo
            .put({
                TableName: "classrooms",
                Item: classroomData
            })
            .promise();
            
            await dynamo
            .delete({
                TableName: "students",
                Key: {
                    id: event.pathParameters.id
                }
            })
            .promise();
            body = `Deleted student ${event.pathParameters.id}`;
            
       
            
            break;
        default:
            throw new Error(`Unsupported route: "${event.routeKey}"`);
        }
    } catch (err) {
        statusCode = 400;
        body = err.message;
    } finally {
        body = JSON.stringify(body);
    }

    return {
        statusCode,
        body,
        headers
    };
};